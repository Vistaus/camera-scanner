#ifndef EXTRACTORCONFIG_H
#define EXTRACTORCONFIG_H

#include <QObject>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include <QFile>
#include <QSqlTableModel>
#include <QtSql>
#include <QVariant>

#include "ONSExtractor.h"

class QSqlDatabase;
class QSqlQuery;

namespace DocumentScanner {

class ExtractorConfig : public QObject
{
  Q_OBJECT

public:
  ExtractorConfig(QObject *parent = 0);
  ~ExtractorConfig();

  void addImageConfig(const QString &id);
  void removeImageConfig(const QString &id);
  void editImageConfig(const QString &id, const ONSExtractorConfig &conf);
  ONSExtractorConfig loadImageConfig(const QString &id);

signals:

private:
  void createDataDir(void);
  void createDB(QString path);
  void createTableSettings(void);
  void closeDB(void);

  QSqlDatabase *m_db;
};
} // namespace DocumentScanner

#endif
